from turtle import *
import turtle
from random import randint

screen = turtle.Screen()
screen.title("Python Turtle")
score = 0
time = 10
game_over = False

score_text=turtle.Turtle()
time_text = turtle.Turtle()

def writeScore():
    global score
    score_text.clear()
    score_text.hideturtle()
    score_text.speed(0)
    score_text.color("dark blue")
    score_text.penup()
    score_text.setposition(0, screen.window_height() / 2 * 0.8)
    score_text.write(arg=f"Score: {score}",move=False,align="center",font=('Arial',30,'normal'))
def writeTime():
    global time
    time_text.hideturtle()
    time_text.speed(0)
    time_text.color("dark violet")
    time_text.penup()
    time_text.setposition(0, screen.window_height() / 2 * 0.7)
    time_text.write(arg=f"Time: {time}", move=False, align="center", font=('Arial', 20, 'normal'))
    if time > 0:
        time_text.clear()
        time_text.write(arg=f"Time: {time}",move=False,align="center",font=('Arial',20,'normal'))
        time = time - 1
        screen.ontimer(lambda: writeTime(), 1000)
    else:
        global game_over
        game_over = True
        time_text.clear()
        time_text.color("red")
        time_text.setposition(0,0)
        time_text.write(arg="Game Over!", move=False, align="center", font=('Arial', 80, 'normal'))

def makeTurtle(x,y):
    global game_over
    if not game_over:
        global score
        score = score + 1
        writeScore()
        t.goto(randint(-200,200),randint(-200,200))
        print(t.pos())

t = turtle.Turtle(shape='turtle')
t.color('green')
t.speed(0)
t.shapesize(2)
t.penup()
game_over = False
t.onclick(makeTurtle)

writeScore()
writeTime()

screen.mainloop()